# Playfusion Deck Sharing

Generate/parse deck sharing codes for [Warhammer Age of Sigmar: Champions](https://www.warhammerchampions.com/) and [Lightseekers](https://www.lightseekers.cards/).

## Require

To require the module
    
    npm install --save @playfusion/deck-sharing

Then import/require using your module loader of choice.

This module is compatible with NodeJS and browser environments.

#### Browser script include

If you want to use it in a browser without a module loader or build process you can include the UMD version (`lib/umd.js`)
and reference `window.playfusionDeckSharing`. You can see an example of such usage in `example.html` in the root of this repo.

To get that file either
 - Build it by cloning this repo and running `npm i && npm run-script build`
 - Download the module using `npm` by doing `npm pack @playfusion/deck-sharing` and extract it from the saved tgz file

#### Browserify example

    // main.js
    var deckSharing = require("@playfusion/deck-sharing")
    deckSharing.generateWarhammer([{id: 123, count: 1}, {id: 234, count: 2}])

then

    browserify main.js -o dist.js

## Generate deck codes

To create a Warhammer deck code:

```javascript
    import {generateWarhammer} from "@playfusion/deck-sharing"
    
    const deck = [
        {id: -276130957, count: 1},
        {id: -1050439153, count: 1},
        {id: -513512609, count: 2},
        // etc ...
    ]
    generateWarhammer(deck) // => _wEDMe15hQLGmUCUA1cFKJgDtVl_nAGJqr-tA-OMw7gBPuDQzAPrs7fNAxvGWdgBSa18GgNEH8YlAzkyzi4BeQibRAPxwBRZAQFyDFoD38VzawFEXxV2
```

and for Lightseekers:

```javascript
    import {generateLightseekers} from "@playfusion/deck-sharing"
    
    const deck = [
        {id: 51377903, count: 1}, // the first item must be the Hero
        {id: 50431448, count: 2},
        {id: 77349409, count: 1},
        // Optionally you may pass a variant, but if omitted variant = 1 is implied
        {id: 12345678, count: 2, variant: 2}
        // etc ...
    ]
    generateLightseekers(deck) // => _wEtXhUD2IUBg4R2A0N5wASDFe4FQxjxBUN2IghDFW4Kg5UKDkMgwQ6D7_YPg889EoP8nBJDeU0TQzvwGEO-GBtDPIkbQ6E3HUNKkCKDb8kiQ3ceJoPjcCaDivAqQ0tbLkNr0z5DIUKcRJkQt0Q
```

Using a generated deck code to link to the app

```html
    <a href="warhammer-tcg://share-deck?_wEDMe15hQLGmUCUA1cFKJgDtVl_nAGJqr-tA-OMw7gBPuDQzAPrs7fNAxvGWdgBSa18GgNEH8YlAzkyzi4BeQibRAPxwBRZAQFyDFoD38VzawFEXxV2&amp;deepLinkTimestamp=1540311920029">
        Open in Warhammer: Age of Sigmar Champions
    </a>
    <a href="lightseekers-tcg://share-deck?_wEtXhUD2IUBg4R2A0N5wASDFe4FQxjxBUN2IghDFW4Kg5UKDkMgwQ6D7_YPg889EoP8nBJDeU0TQzvwGEO-GBtDPIkbQ6E3HUNKkCKDb8kiQ3ceJoPjcCaDivAqQ0tbLkNr0z5DIUKcRJkQt0Q&amp;deepLinkTimestamp=1540311920029">
        Open in Lightseekers
    </a>
```

### A note on ordering

For Warhammer the order of the cards you pass in to the generate function is irrelevant, they are sorted before the deck code is generated.
This means 2 decks which are semantically identical but expressed differently will result in identical deck codes.

For Lightseekers the first card in the list passed in is assumed to be the hero and treated differently.
The rest of the cards are then sorted before the code is generated.

## Parse deck codes

To parse a deck code from Warhammer:

```javascript
    import {parseWarhammer} from "@playfusion/deck-sharing"
    
    const deck = parseWarhammer("_wEtXhUD2IUBg4R2A0N5wASDFe4FQxjxBUN2IghDFW4Kg5UKDkMgwQ6D7_YPg889EoP8nBJDeU0TQzvwGEO-GBtDPIkbQ6E3HUNKkCKDb8kiQ3ceJoPjcCaDivAqQ0tbLkNr0z5DIUKcRJkQt0Q")
    // =>
    // [
    //  {id: -276130957, count: 1},
    //  {id: -1050439153, count: 1},
    //  {id: -513512609, count: 2},
    //  ...
    // ]
```

To parse a deck code from Lightseekers:

```javascript
    import {parseLightseekers} from "@playfusion/deck-sharing"
    
    const deck = parseLightseekers("_wEDMe15hQLGmUCUA1cFKJgDtVl_nAGJqr-tA-OMw7gBPuDQzAPrs7fNAxvGWdgBSa18GgNEH8YlAzkyzi4BeQibRAPxwBRZAQFyDFoD38VzawFEXxV2")
    // =>
    // [
    //  {id: 51377903, count: 1, hero: true},   <-- the Hero will always be first and have hero: true set
    //  {id: 50431448, count: 2},
    //  {id: 77349409, count: 1},
    //  ...
    // ]
```

You can then use these ids against the card data API to get full card data.

## Validation

There is no validation!

On generating a deck code all that is validated is that you have provided a list of cards each of which has an `id` and a `count` property.
Other than that you are free to generate deck codes which represent illegal decks, e.g. too many or too few cards, or use ids of cards which do not exist.
It is up to you to validate these things before generating a deck code.
Of course such invalid decks can't be opened in the digital game and will simply be met with an error message.

On parsing deck code provided to you an error will be thrown if the deck code format is invalid.
Beyond that you have no guarantee that there will be a legal number of cards (it could be zero or a million!) or that the card ids are real.

## Bugs & Feedback

If you spot any issues or have any feedback, please get in touch via our
[official Discord channel](https://discord.gg/J59SZNN) using the #playfusion-feedback channel.