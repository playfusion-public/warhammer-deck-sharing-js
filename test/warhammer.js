const assert = require("assert")
const {parseWarhammer, generateWarhammer} = require("../lib/index")
const _ = require("lodash")

const testDeck = [
  {id: -276130957, count: 1},
  {id: -1050439153, count: 1},
  {id: -513512609, count: 2}
]

// Generate and then parse
const code = generateWarhammer(testDeck)
assert(code === "_wEBD5JjwQJfa2ThAXOTiu8=")

assert(_.isEqual({
  version: 1,
  cards: testDeck
}, parseWarhammer(code)))

// Check generated codes use sorted deck
const shuffledTestDeck = [
  testDeck[1],
  testDeck[0],
  testDeck[2]
]
assert(_.isEqual(code, generateWarhammer(shuffledTestDeck)))

// Parse some predefined decks
assert(_.isEqual({
    version: 1,
    cards: [
      {count: 2, id: -1907575213},
      {count: 1, id: -1865513375},
      {count: 1, id: -1394433650},
      {count: 1, id: -1358221892},
      {count: 2, id: -1296548817},
      {count: 2, id: -1006354164},
      {count: 1, id: -579269206},
      {count: 1, id: -386968580},
      {count: 1, id: -133458063},
      {count: 1, id: -54414003},
      {count: 1, id: 41486326},
      {count: 1, id: 186210356},
      {count: 1, id: 301461669},
      {count: 1, id: 551420610},
      {count: 2, id: 718575324},
      {count: 2, id: 782999823},
      {count: 2, id: 953437043},
      {count: 2, id: 960390689},
      {count: 1, id: 1030532525},
      {count: 2, id: 1061665152},
      {count: 2, id: 1079596048},
      {count: 1, id: 1167834319},
      {count: 1, id: 1525131750},
      {count: 1, id: 1662048061},
      {count: 2, id: 1802749407},
      {count: 2, id: 2043683639},
      {count: 1, id: 2068770215}
    ]
  },
  parseWarhammer("_wECU7ZMjgFhhs6QAY6h4qwBvC0LrwIvPLiyAgxBBMQBqg153QH8U-_oAXGXC_gBTbXB_AH2B3kCATRYGQsBpfD3EQHCAt4gAtyW1CoCD6GrLgJzS9Q4AiFmPjkBra1sPQKAuUc_AhBUWUABz7ybRQHmqedaAT3XEGMC38VzawI3I9B5AaftTns")
))

assert(_.isEqual({
    version: 1,
    cards: [
      {count: 3, id: -2055606991},
      {count: 2, id: -1807705658},
      {count: 3, id: -1742207657},
      {count: 3, id: -1669375563},
      {count: 1, id: -1379947895},
      {count: 3, id: -1195143965},
      {count: 1, id: -858726338},
      {count: 3, id: -843598869},
      {count: 3, id: -665205221},
      {count: 1, id: 444378441},
      {count: 3, id: 633741124},
      {count: 3, id: 785265209},
      {count: 1, id: 1151010937},
      {count: 3, id: 1494532337},
      {count: 1, id: 1510765057},
      {count: 3, id: 1802749407},
      {count: 1, id: 1981112132}
    ]
  },
  parseWarhammer("_wEDMe15hQLGmUCUA1cFKJgDtVl_nAGJqr-tA-OMw7gBPuDQzAPrs7fNAxvGWdgBSa18GgNEH8YlAzkyzi4BeQibRAPxwBRZAQFyDFoD38VzawFEXxV2")
))

console.log("Warhammer tests passed!")