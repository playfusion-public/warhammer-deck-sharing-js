"use strict"

/*
Format of deck codes version 1:
<byte 0xFF>, <byte version>, [<byte count><32 bit id>, <byte count><32 bit id>, ...]
 */

const IN_BROWSER = typeof window !== "undefined"
const GAMES = {
  WARHAMMER: {
    name: "warhammer",
    maxSupportedVersion: 1
  },
  LIGHTSEEKERS: {
    name: "lightseekers",
    maxSupportedVersion: 2
  }
}

function fromBase64 (b64) {
  return IN_BROWSER ? window.atob(b64) : Buffer.from(b64, "base64").toString("latin1")
}

function toUrlSafeBase64 (str) {
  let b64 = IN_BROWSER ? window.btoa(str) : Buffer.from(str, "latin1").toString("base64")
  return b64.replace(/\//g, "_").replace(/\+/g, "-")
}

function getVersionAndData (raw) {
  const buffer = new ArrayBuffer(raw.length)
  const view = new DataView(buffer)
  for (let i = 0; i < raw.length; i++) {
    view.setUint8(i, raw.charCodeAt(i))
  }
  let version = 0
  const magicNumber = view.getUint8(0)
  if (magicNumber === 0xFF) {
    version = view.getUint8(1)
  }
  return {
    version,
    data: new DataView(buffer, version === 0 ? 0 : 2)
  }
}

function getWarhammerCardsV1 (data) {
  const cards = []
  const bytesPerCard = 5
  for (let i = 0; i < data.byteLength; i += bytesPerCard) {
    cards.push({
      count: data.getUint8(i),
      id: data.getInt32(i + 1, true)
    })
  }
  return cards
}

function getLightseekersCardsV1 (data) {
  const cards = []
  // Get the hero first
  cards.push({
    count: 1,
    hero: true,
    id: data.getInt32(0, true)
  })
  // Then the rest of the cards
  const bytesPerCard = 4
  for (let i = bytesPerCard; i < data.byteLength; i += bytesPerCard) {
    const card = data.getInt32(i, true)
    cards.push({
      count: card >> 30 & 0x3,
      id: card & ~(0x3 << 30)
    })
  }
  return cards
}

function getLightseekersCardsV2 (data) {
  const cards = []
  // Get the hero first
  cards.push({
    count: 1,
    hero: true,
    id: data.getInt32(0, true),
    variant: data.getInt8(4)
  })
  // Then the rest of the cards
  const bytesPerCard = 5
  for (let i = bytesPerCard; i < data.byteLength; i += bytesPerCard) {
    const card = data.getInt32(i, true)
    cards.push({
      count: card >> 30 & 0x3,
      id: card & ~(0x3 << 30),
      variant: data.getInt8(i + 4)
    })
  }
  return cards
}

function parseQueryStringDeckCode (qsDeckCode) {
  return qsDeckCode.replace(/_/g, "/").replace(/[- ]/g, "+")
}

function getCardsParser (game, version) {
  switch (game) {
    case GAMES.WARHAMMER:
      return getWarhammerCardsV1
    case GAMES.LIGHTSEEKERS:
      return version === 1 ? getLightseekersCardsV1 : getLightseekersCardsV2
    default:
      throw new Error("Unknown game: " + game)
  }
}

function generateWarhammerCards (cards, view, initialOffset) {
  const sortedCards = cards.sort((a, b) => b.id > a.id ? -1 : 1)
  sortedCards.forEach(({id, count}, i) => {
    if (!id) throw new Error("Missing card id at index " + i)
    if (!count) throw new Error("Missing or zero card count at index " + i)
    const offset = initialOffset + (i * 5)
    view.setUint8(offset, count)
    view.setUint32(offset + 1, id, true)
  })
}

function generateLightseekersCards (cards, view, initialOffset) {
  const [hero, ...otherCards] = cards
  view.setInt32(initialOffset, hero.id, true)
  view.setUint8(initialOffset + 4, 1)
  const sortedCards = otherCards.sort((a, b) => b.id > a.id ? -1 : 1)
  sortedCards.forEach(({id, count, variant = 1}, i) => {
    if (!id) throw new Error("Missing card id at index " + i)
    if (!count) throw new Error("Missing or zero card count at index " + i)
    const offset = initialOffset + ((i + 1 /* as hero is set already */) * 5)
    const data = (count << 30) | id
    view.setUint32(offset, data, true)
    view.setUint8(offset + 4, variant)
  })
}

function getCardsGenerator (game) {
  switch (game) {
    case GAMES.WARHAMMER:
      return generateWarhammerCards
    case GAMES.LIGHTSEEKERS:
      return generateLightseekersCards
    default:
      throw new Error("Unknown game: " + game)
  }
}

/**
 * Given a deck code, returns an object with the deck code version, and an array of objects like {id: <card id>, count: <card count>}
 * @param deckCode
 * @return {{version: number, cards: [{id: number, count: number}]}}
 */
function parse (game, deckCode) {
  const raw = fromBase64(parseQueryStringDeckCode(deckCode))
  const {version, data} = getVersionAndData(raw)
  if (version > game.maxSupportedVersion) {
    throw new Error("Unsupported deck sharing version: " + version)
  }
  return {
    version,
    cards: getCardsParser(game, version)(data)
  }
}

/**
 * Generate a deck code from a list of cards and their counts
 * @param {array} cards  Each element of the array should be an object containing the card id and count, e.g. {id: 123456, count: 2} (other keys are ignored)
 * @return {string}      Deck code base64 encoded which can be imported into the game
 */
function generate (game, cards) {
  const bytesPerCard = 5
  // Sort the cards for consistency, so the same deck in a different order produces the same deck code
  const buffer = new ArrayBuffer(2 + cards.length * bytesPerCard)
  const view = new DataView(buffer)
  // Set magic number and version byte
  view.setUint8(0, 0xFF)
  view.setUint8(1, game.maxSupportedVersion)
  // Set cards
  getCardsGenerator(game)(cards, view, 2)
  // Convert to a base64 string
  const str = String.fromCharCode.apply(null, new Uint8Array(buffer))
  return toUrlSafeBase64(str)
}

module.exports = {}

// Create some bound/game-named versions of parse/generate to export
// e.g. parseLightseekers and generateWarhammer
const bindFns = {parse, generate}
Object.values(GAMES).forEach(game => {
  const gameName = game.name
  Object.entries(bindFns).forEach(([fnName, fn]) => {
    // Original fn name + game name, camel cased
    const boundName = fnName + gameName.slice(0, 1).toUpperCase() + gameName.slice(1)
    module.exports[boundName] = fn.bind(null, game)
  })
})